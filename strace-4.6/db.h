#ifndef _DB_H
#define _DB_H

#include "../leveldb-1.14.0/include/leveldb/c.h"
#include "sqlite3.h"

#ifndef _typedef_db_t
#define _typedef_db_t
typedef struct {
  sqlite3 *dbp;
  leveldb_t *db;
  leveldb_options_t *options;
  leveldb_writeoptions_t *woptions;
  leveldb_readoptions_t *roptions;
} db_t;
#endif

#ifndef ULL_T
#define ULL_T
typedef long long int ull_t;
#endif

// typedef sqlite3* db_t;

// init db
void db_init(char *path, db_t *mydb);
void db_close(db_t *mydb);

// basic operations
void db_write(db_t *mydb, const char *key, const char *value);
void db_write_fmt(db_t *mydb, const char *key, const char *fmt, ...);
char* db_nread(db_t *mydb, const char *key, size_t *plen);
int db_read_ull(db_t *mydb, const char *key, ull_t* pvalue);
char* db_readc(db_t *mydb, const char *key);

// pid
char* db_read_pid_key(db_t *mydb, long pid);
void db_write_root(db_t *mydb, long pid);

// io
void db_write_iofd_prov(db_t *mydb, long pid, int prv, const char *filename_abspath, int fd);
ull_t db_write_io_prov(db_t *mydb, long pid, int prv, const char *filename_abspath);
int db_markFileClosed(db_t *mydb, long pid, int fd);

// exec
void db_write_exec_prov(db_t *mydb, long ppid, long pid, const char *filename_abspath, \
    char *current_dir, char *args, char *dbid, char *ssh_host);
void db_write_execdone_prov(db_t *mydb, long ppid, long pid, char* env_str, int env_len);
void db_write_spawn_prov(db_t *mydb, long ppid, long pid);

// exit
void db_write_lexit_prov(db_t *mydb, long pid);
void db_write_iexit_prov(db_t *mydb, long pid);

// stat
void db_write_prov_stat(db_t *mydb, long pid, const char* label, char *stat);

// sock read/write
void db_write_sock_action(db_t *mydb, long pid, int sockfd, \
    const char *buf, long len_param, int flags, \
    long len_result, int action, void *msg);
ull_t db_getPkgCounterInc(db_t *mydb, char* pidkey, char* sockid, int action);
char* db_getSendRecvResult(db_t *mydb, int action, 
    char* pidkey, char* sockid, ull_t pkgid, ull_t *presult, void *msg);

// sock connect
void db_write_connect_prov(db_t *mydb, long pid, 
    int sockfd, char* addr, int addr_len, long u_rval, char *ips);
void db_setupConnectCounter(db_t *mydb, char* pidkey);
ull_t db_getConnectCounterInc(db_t *mydb, char* pidkey);
int db_getSockResult(db_t *mydb, char* pidkey, int sockid);
void db_setSockConnectId(db_t *mydb, char* pidkey, int sock, ull_t sockid);
void db_setupSockConnectCounter(db_t *mydb, char *pidkey, int sockfd, ull_t sockid);

// sock listen
void db_write_listen_prov(db_t *mydb, int pid, 
    int sock, int backlog, int result);
void db_setupListenCounter(db_t *mydb, char* pidkey);
ull_t db_getListenId(db_t *mydb, char* pidkey, int sock);
ull_t db_getListenCounterInc(db_t *mydb, char* pidkey);
int db_getListenResult(db_t *mydb, char* pidkey, ull_t id);
void db_setListenId(db_t *mydb, char* pidkey, int sock, ull_t sockid);

// sock accept
void db_write_accept_prov(db_t *mydb, int pid, int lssock, 
    char* addrbuf, int len, ull_t client_sock, char* ips);
void db_setupAcceptCounter(db_t *mydb, char* pidkey, ull_t listenid);
ull_t db_getAcceptCounterInc(db_t *mydb, char* pidkey, ull_t listenid);
void db_setSockAcceptId(db_t *mydb, char* pidkey, int sock, ull_t listenid, ull_t acceptid);
void db_setupSockAcceptCounter(db_t *mydb, char *pidkey, int sockfd, ull_t listenid, ull_t acceptid);

// sock captured/ignored
void db_setCapturedSock(db_t *mydb, int sockfd);
void db_removeCapturedSock(db_t *mydb, int sockfd);
int db_isCapturedSock(db_t *mydb, int sockfd);
// ---
char* db_getSockId(db_t *mydb, char* pidkey, int sock);
void db_remove_sock(db_t *mydb, long pid, int sockfd);

// others
uint32_t checksum(const void *buf, size_t buflength);
void printbuf(const char *buf, size_t buflength);

void db_write_getsockname_prov(db_t *mydb, int pid, int sock, char* addrbuf, int len, ull_t res);
void db_get_pid_sock(db_t *mydb, long pid, int sockfd, char **pidkey, char **sockid);
char *db_read_real_pid_key(db_t *mydb, long pid);

// env
char* db_getEnvVars(db_t *mydb, char* pidkey);

// remote host
int db_hasPTUonRemoteHost(db_t *mydb, char* remotehost);
void db_setPTUonRemoteHost(db_t *mydb, char* remotehost);
int db_get_ssh_host(db_t *mydb, long pid, char** host, char** dbid);

// misc
void findKey(db_t *mydb, char* key, char* p);
void db_io_write(db_t *mydb, long pid, int fd, const char *buff, int len);
void db_io_fopen(db_t *mydb, long pid, const char *path);
#endif // _DB_H
