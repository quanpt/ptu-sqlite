Install:
+ ./configure
+ make

SQLite:
+ select * from process where pidkey="meta" and attr="root"; ==> root pidkey
+ select * from process where pidkey="_a_pidkey_"; ==> get info of that pidkey
    
    Example:
    sqlite> select * from process where pidkey="10849.1423446790238300";
    - 10849.1423446790238300|args|1423446790238300|["python", "exp.py"]
    - 10849.1423446790238300|env|1423446790303186|XDG_VTNR=7
    - 10849.1423446790238300|iexit|1423446792471117|1423446792471117
    - 10849.1423446790238300|ok|1423446790303186|1423446790303186
    - 10849.1423446790238300|path|1423446790238300|/usr/bin/python
    - 10849.1423446790238300|pwd|1423446790238300|/home/quanpt/assi/ptu.new/mytest/python
    - 10849.1423446790238300|start|1423446790238300|1423446790238300

+ select * from process where pidkey="_a_pidkey_" and (attr="exec" or attr="spawn"); ==> all child pidkeys
    - exec ==> pidkey with all above attributes (resulted from an execv() syscall)
    - spawn ==> pidkey without those attributes (resulted from a fork() syscall)


+ select * from access where pidkey="_a_pidkey_"; ==> get files access by that pidkey

Performance notes:
+ a simple python script run with ptu+leveldb in 0.22 seconds
+ same script, with ptu+leveldb+sqlite: 2.2 seconds (9 rows in process, 80 rows in access)
+ same script, with ptu+leveldb+sqlite and mirror everything from leveldb: 13 seconds

Things to improve performance:
+ try re-use prepared statements?
+ write data to text file and bulk import to SQLite later?

***

PTU manual can be found in PTU website.

CDE: Code, Data, and Environment packaging for Linux
http://www.stanford.edu/~pgbovine/cde.html

CDE is currently licensed under GPL v3:

  Copyright (c) 2010-2011 Philip Guo <pg@cs.stanford.edu>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

---

CDE is heavily built upon strace, whose license is in this file:
strace-4.6/COPYRIGHT

Copyright (c) 1991, 1992 Paul Kranenburg <pk@cs.few.eur.nl>
Copyright (c) 1993 Branko Lankester <branko@hacktic.nl>
Copyright (c) 1993 Ulrich Pegelow <pegelow@moorea.uni-muenster.de>
Copyright (c) 1995, 1996 Michael Elizabeth Chastain <mec@duracef.shout.net>
Copyright (c) 1993, 1994, 1995, 1996 Rick Sladkey <jrs@world.std.com>
Copyright (C) 1998-2001 Wichert Akkerman <wakkerma@deephackmode.org>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

---

CDE also uses the libreadelf-mini.a library within the readelf-mini/
directory, which is a modified version of the readelf program from GNU
binutils-2.20.1.  It carries the following license, as written in
readelf-mini/readelf-mini.c :

/* readelf.c -- display contents of an ELF format file
   Copyright 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007,
   2008, 2009  Free Software Foundation, Inc.

   Originally developed by Eric Youngdale <eric@andante.jic.com>
   Modifications by Nick Clifton <nickc@redhat.com>

   This file is part of GNU Binutils.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA
   02110-1301, USA.  */

