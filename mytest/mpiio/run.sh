#!/bin/bash
rm -rf cde-package test

#mpirun -n 2  ./mpi_hello_world
time ../../ptu $@ ./exp.sh

### post-process db for indirect (spawn) links
../../scripts/db2dot.py -f cde-package/provenance.cde-root.1.log -d gv1 > /dev/null 2>&1
